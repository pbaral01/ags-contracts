import os
import logging
from logging import Formatter

import pandas as pd
from flask import Flask, render_template, request, send_file
from werkzeug.utils import secure_filename
from flask_cors import CORS
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash


from model.model import contract_extract
from model.gcs import upload_to_gcs
#---------------------------------------------

app = Flask(__name__)
CORS(app)

auth = HTTPBasicAuth()
model = contract_extract('ags_round5')

users = {
    "ags": generate_password_hash("Dsiarocks"),
    "dsia": generate_password_hash("Allegisrocks")
}

log = logging.getLogger('flaskapp')
handler = logging.StreamHandler()

handler.setFormatter(Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'
))

log.addHandler(handler)
log.setLevel(logging.WARNING)
#--------------------------------------------------


@auth.verify_password
def verify_password(username: str, password: str):
	'''
	this  method will verify the hash of username and password
	parameters:
		username: username of the username
		password: the password of the user
	returns:
		username or None
	'''
	if username in users and check_password_hash(users.get(username), password):
		return username

@app.route('/upload')
@auth.login_required
def home_page():
	'''
	this method will render the UI

	parameters:
		None
	returns:
		the rendered html page for the UI
	'''
	return render_template('index.html')
	
@app.route('/uploader', methods = ['GET', 'POST'])
@auth.login_required
def upload_files():
	'''
	this method will upload the files selected and route them to the model to extract the contract items
	
	parameters:
		None
	
	returns:
		excel document with all the extracted parameters for the contracts uploaded
	'''
	log.info("entered upload_files method")
	if request.method == 'POST':
		df = pd.DataFrame([[None]*16])
		df.columns = ['filename', 'sow_num', 'sow_name', 'agreement_number', 'parties', 'buyer', 'seller', 'start_date', 'start_date_cleaned', 'end_date', 'end_date_cleaned', 'sow_duration', 'max_budget', 'max_budget_cleaned', 'currency', 'payment_term']
		
		for file_ in request.files.getlist('file'):
			filename = file_.filename
			[os.remove(os.path.join('file_dump', files)) for files in os.listdir('file_dump')]
			file_.save(os.path.join(os.getcwd(),'file_dump', secure_filename(filename)))
			filename = os.listdir('file_dump')[0]
			if file_.filename.endswith('.docx') or file_.filename.endswith('.pdf'):
				try:
					upload_to_gcs(os.path.join(os.getcwd(),"file_dump", filename))
				except:
					log.error("error connecting to gcs")

				try:

					sow_num, sow_name, agreement_number, parties, buyer, seller, start_date, start_date_cleaned, end_date, end_date_cleaned, sow_duration, max_budget, max_budget_cleaned, currency, payment_term = model.predict(os.path.join(os.getcwd(),"file_dump", filename))
					
					append_ = pd.Series([filename, sow_num, sow_name, agreement_number, parties, buyer, seller, start_date, start_date_cleaned, end_date, end_date_cleaned, sow_duration, max_budget, max_budget_cleaned, currency, payment_term], index = df.columns)
					df = df.append(append_, ignore_index=True)
				except Exception as e:
					print(e)
					data = f'''error: {e} <br><br><br>
					<form>
					 <input type="button" value="Try Another Contract" onclick="history.back()">
					</form>'''

		#[os.remove(os.path.join('file_dump', files)) for files in os.listdir('file_dump')]
		df.to_excel('file_dump/output.xlsx', index = False)
		result = send_file('file_dump/output.xlsx', as_attachment=True)
		log.info("exited the upload method")
	return result
	
	
	
		
if __name__ == '__main__':
	app.run(host = '0.0.0.0', port = 5000, debug=True)