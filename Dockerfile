FROM python:3.6-slim

RUN mkdir -p /home/pbaral/ags_model

COPY .  /home/pbaral/ags_model/


WORKDIR /home/pbaral/ags_model

# make sure to copy the key file into the docker image as it is being built 
# removing the key from bitbucket repo for security reasons
# ENV GOOGLE_APPLICATION_CREDENTIALS=/home/pbaral/ags_model/####path to the key file ###

RUN pip install lexnlp==1.8.0

RUN pip install -r requirements.txt

RUN python -m nltk.downloader popular

EXPOSE 5000

CMD exec gunicorn --bind 0.0.0.0:5000 app:app --timeout 10000