from google.cloud import storage
import os



def upload_to_gcs(file_):
	'''
	this method uploads a file to google cloud storage within ags-bucket/ags_secure_uploads/file
	params:
		file: path to thefile to be uploaded
	returns None
	'''
	separator = os.path.sep
	client = storage.Client()
	bucket = client.get_bucket('ags-bucket')
	blob = bucket.blob('ags_secure_uploads/'+file_.split(separator)[-1])
	blob.upload_from_filename(filename=file_)
	return None
	