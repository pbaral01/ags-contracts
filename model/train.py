import os
import warnings
warnings.filterwarnings('ignore')
import random
import datetime

import pandas as pd
import spacy
from spacy.util import minibatch, compounding
from spacy.gold import GoldParse
from spacy.scorer import Scorer

def train_spacy_ner_model(train_data, 
                          test_data, 
                          n_iter, 
                          labels, 
                          model_save_path = str(datetime.datetime.now().date()), 
                          dropout = .35,
                          print_logs_every=10,
                          save_every = 1):
    '''
    this methods trains and saves a spacy ner model
    
    inputs:
        train_data: labelled training data in spacy format
        test_data: labelled test data in spacy accepted format
        n_iters: the number of epochs to train for (int)
        labels: a list of entities the NER is supposed to be trained
        model_save_name: model will be saved as this name after training is done
    optional inputs:
        dropout: ranges between 0-1 and will be used as a penalty parameter for regularization
        print_logs_every: training and testing metrics will be output to stdout only once every "print_logs_every" epochs
        
    returns: None
    '''
    test_df_metrics = pd.DataFrame()
    train_df_metrics = pd.DataFrame()
	
    nlp = spacy.blank('en')  
    ner = nlp.create_pipe('ner')
    nlp.add_pipe(ner)
    
    if not os.path.exists(model_save_path):
        os.makedirs(model_save_path, exist_ok=True)

    for i in labels:
        ner.add_label(i) 
    
    optimizer = nlp.begin_training()
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  
        for itn in range(n_iter):
            random.shuffle(train_data)
            losses = {}
            batches = minibatch(train_data, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=dropout,losses=losses)
                

            # save model
            if itn % save_every == 0:

                model_save_name= f"{model_save_path}_epoch_{itn}"
                nlp.meta['name'] = model_save_name  
                nlp.to_disk(f"{model_save_path}/{model_save_name}")
            
            # append metrics
            test_df_metrics = test_df_metrics.append(
                evaluate(ner_model = nlp, examples = test_data, flatten = True, epoch = itn),
                ignore_index = True)
				
            train_df_metrics = train_df_metrics.append(
                evaluate(ner_model = nlp, examples = train_data, flatten = True, epoch = itn),
                ignore_index = True)
            
            
            # print metrics
            if itn % print_logs_every == 0:
                print(f'test_results in epoch {itn+1}: \n', test_df_metrics.iloc[itn])
                print(f'train results in epoch {itn+1}: \n', train_df_metrics.iloc[itn])
                print("-----------------------------------------------------------------------\n")
                
        test_df_metrics.to_csv(f"{model_save_path}/test_metrics_per_epoch.csv", index = False)
        train_df_metrics.to_csv(f"{model_save_path}/train_metrics_per_epoch.csv", index = False)
        
    return None

def evaluate(ner_model, examples, flatten = False, epoch = None):
    '''
    takes in an NER model and returns the metrics (f1 score, precision, recall)
    args:
        ner_model: a spacy ner model
        examples: the examples which we want to evaluate the model on
        flatten (optional):  if flatten, returns a non-nested dictionary
        epoch (optional):    if user wants to log the metrics every epoch, pass it as an argument    
        
    returns:
        a dictionary with metrics (precision, recall, f1-score) per entity 
    
    '''
    scorer = Scorer()
    for input_, annot in examples:
        annot_ = annot['entities']
        doc_gold_text = ner_model.make_doc(input_)
        
        gold = GoldParse(doc_gold_text, entities=annot_)
        pred_value = ner_model(input_)
        scorer.score(pred_value, gold)
        results = scorer.scores
        results = results['ents_per_type']
        
        if flatten:
            metrics = {"epoch": epoch}
            for entity in results.keys():
                for metric in results[entity].keys():
                    metrics[f"{entity}_{metric}"] = results[entity][metric]
        
        else:
            metrics = results
            
    return metrics
