import os
import re

import pandas as pd
from google.cloud import storage


from preprocessing.documents_to_text import extract_pdf

def getIndex(content,text_to_find):
    """
    This method is used to get index of the corrected entities in the corresponding contract
    params:
        content: the content of the contract
        text_to_find: the correction in the excel file
        
    returns a list of tuple start and end index [(start_0,end_0),(start_1,end_1),...]
    """
    text_to_find=text_to_find.replace('$','\\$') if '$' in text_to_find else text_to_find
    return [(m.start(0), m.end(0)) for m in re.finditer(text_to_find, content,re.IGNORECASE)]

def getEntityList(content,tuple_dictionary):
    
    """
    This method is used to get training data for each contract with indices for corrected labels
    params:
        content: the content of the contract
        tuple_dictionary: a dictionary for each entity as a key and list of start and index for the label results in this format: 
        {"sow_num":[(0,15)],"buyer":[(200,210),(300,310)],}
        
    returns a tuple with the format (content,entities dictionary)
    """
    entity_list=[]                                                             
            
    for key,value in tuple_dictionary.items(): 
        for j in range(0,len(value)):  
            entity_list.append((value[j][0],value[j][1],key))  
    
    return (content,{"entities":entity_list})
    
def getTupleDictionary(content,entity_dictionary):
    """
    This method is used to get a dictionary for each entity as a key and list of start and index for the label
    params:
        content: the content of the contract
        entity dictionary: a dictionary of entities with keys as entities and value of the entity as values. eg:
            {"sow_num": "00123456", "buyer": "microsoft inc"}
     returns results in this format: {"sow_num":[(0,15)],"buyer":[(200,210),(300,310)],}
    """
    tuple_dictionary={}                                                        
    for key,value in entity_dictionary.items():                                
        if(value!='nan'):                                                      
            tuple_dictionary[key]=getIndex(content,value)               
            
    return tuple_dictionary
    
def gcsFileExists(filename):
    """
    This method is used to check if the file exists in google cloud storage @ gs://ags-bucket/ags_secure_uploads/
    params:
        filename: the filename of the contract to check
    returns True if file exists else false
    """
    gcs_path="gs://ags-bucket/ags_secure_uploads/"
    client = storage.Client()
    bucket = gcs_path.split("/")[2]
    path_to_file=gcs_path.split(bucket+"/")[1:][0]
    bucket = client.bucket(bucket)
    stats = storage.Blob(bucket=bucket, name=path_to_file+filename).exists(client)
    return stats

def correct_labels(excel_file, sheet_name = "Sheet1"):
    """
    This method takes is used to get corrected training data from the list of corrected entities for each file in the excel document
    params:
        excel_file: the corrected excel file from the business
        sheet_name: the sheet where the corrections are entered. Default value = 'Sheet1'
    returns a list in following format
    [
    ("content of contract", {'entities': [(start_index, end_index, entity1),(start_index, end_index, entity2) ]}),
    ("content of contract", {'entities': [(start_index, end_index, entity1),(start_index, end_index, entity2) ]}),
    ("content of contract", {'entities': [(start_index, end_index, entity1),(start_index, end_index, entity2) ]}),
    ...
    ]
    """
    try:
        df = pd.read_excel(io=excel_file, sheet_name=sheet_name)                         
    except:
        raise Exception("error reading excel file with corrections. Please check the path and/or contents")
        
    training_data=[]                                                               
    df=df[df.filename.notna()].reset_index()                                       
    for i in range(0,df.filename.size):
        try:
            if(gcsFileExists(df['filename'][i])):
                content = extract_pdf("gs://ags-bucket/ags_secure_uploads/"+df['filename'][i],gcs_path=True) 
                entity_dictionary={"sow_num":str(df["sow_num"][i]).rstrip(".0"),
                                   "agreement_number":str(df["agreement_number"][i]),
                                   "buyer":str(df["buyer"][i]),
                                   "seller":str(df["seller"][i]),
                                   "start_date":str(df["start_date"][i]),
                                   "end_date":str(df["end_date"][i]),
                                   "sow_duration":str(df["sow_duration"][i]),
                                   "max_budget":str(df["max_budget"][i]),
                                   "payment_term":str(df["payment_term"][i])}              
                tuple_dictionary=getTupleDictionary(content,entity_dictionary)        
                corrected_data=getEntityList(content,tuple_dictionary)
                training_data.append(corrected_data)                                       
            else:
                continue
                #print("file does not exist")
        except Exception as e:
            print("Exception "+str(e)+ "has occured!!!")
    #print("Training data= {0}".format(training_data))
    return training_data                              

def replace_correction(existing_train_data_all, feedback_train_data):
    '''
    this method replaces correction in existing train data with feedback training data if the content exists. If the content does not exist,
    it appends the feedback training data to existing training data
    
    params:
        existing_train_data_all: the existing training data in spacy format
        feedback_train_data: the training data from corrected excel file
    
    '''
    for feedback_data in feedback_train_data:
        flg=False
        for i,e_data in enumerate(existing_train_data_all):

            if feedback_data[0]==e_data[0]:
                del existing_train_data_all[i]
                existing_train_data_all.append(feedback_data)
                flg = True

        if flg==False:
            existing_train_data_all.append(feedback_data)

    return existing_train_data_all