## History ##

### 7/26/2021 - Release Version 1.0 with features: ###

* extract sow_num, sow_name, agreement_num, parties, buyer, seller, start_date,	end_date, sow_duration, max_budget, currency, and payment_term from contracts

* dates are cleaned up into mm/dd/yyyy format

* payment terms and currency are extracted using regex

* lexnlp used to extend coverage of buyers
