import re

def company_condition(text) -> bool:
    '''
    this method is used to check if the extracted company is potentially a buyer. it checks :
		1. if the extracted company contains the word 'easi'
		2. if it contains the name of a state (sometimes the lexnlp extraction methods returns Maryland LLC as a company for cases like Easi, a maryland LLC
	
	input parameters:
		text: extracted text supposed to be name of a company
		returns: True if it appears to be a non-easi company name with none of the states' names in the company name
		
	#Future Improvement: fuzzy matching to Easi and other allegis business entities that are always 'sellers'
	#Future Improvement: Find a way to filter out legitimate companies with the name of the state in their company names
	
    '''
    text = text.lower()
    if 'easi' in text.replace(" ", ""):
        return False
    for state in states():
        if state in text:
            return False
    return True


def has_number(text: str) -> bool:
    '''
    this method is used to check if any input string has digits in it
	
	input parameters:
		text: input string
	returns:
		True if it contains a digit, False otherwise
    '''
    return any(ch.isdigit() for ch in text)

def extract_sow_num_regex(text: str) -> str:
    '''
    this method is used for extracting SOW numbers that match certain  patterns 
	
	input_parameters:
		text: input string, usually the content of a contract
	
	returns: 
		SOW number if it matches one of the three patterns as explored
    '''
    regex_list = [r"O-[0-9]{7}", r"0-[0-9]{7}", r"40000000[0-9]{7}"]
    regex_matches = [re.findall(regex, text) for regex in regex_list]
    
    if any(regex_matches):
        return [item for item in regex_matches if item][0][0]

    return None

state_ = '''Alabama
Alaska
Arizona
Arkansas
California
Colorado
Connecticut
Delaware
Florida
Georgia
Hawaii
Idaho
Illinois
Indiana
Iowa
Kansas
Kentucky
Louisiana
Maine
Maryland
Massachusetts
Michigan
Minnesota
Mississippi
Missouri
Montana
Nebraska
Nevada
New Hampshire
New Jersey
New Mexico
New York
North Carolina
North Dakota
Ohio
Oklahoma
Oregon
Pennsylvania
Rhode Island
South Carolina
South Dakota
Tennessee
Texas
Utah
Vermont
Virginia
Washington
West Virginia
Wisconsin
Wyoming'''

def states():
    '''
    this method
    '''
    return state_.lower().split("\n")